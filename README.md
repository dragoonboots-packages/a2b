A2B
===

[![pipeline status](https://gitlab.com/dragoonboots-packages/a2b/badges/master/pipeline.svg)](https://gitlab.com/dragoonboots-packages/a2b/-/commits/master)

[![coverage report](https://gitlab.com/dragoonboots-packages/a2b/badges/master/coverage.svg)](https://gitlab.com/dragoonboots-packages/a2b/-/commits/master)

A2B is an Extract/Transform/Load tool for Symfony.  Features include:
- Built-in and custom sources and destinations
- Tracks previously migrated data, allowing old data to remain in use while new data is prepared
- Supports complex data sources where one row may reference another

Read the official documentation [here](https://dragoonboots-packages.gitlab.io/a2b)
